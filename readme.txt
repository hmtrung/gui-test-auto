BasicProjecAutomation
---------------------
Copyright (c) 2016 Flexera Software LLC. All rights reserved.
------------------------------------------------------------------------------------------------

This sample Eclipse project shows how to use the GUIAutomationFixture in order to create JUnit tests 
that can drive the GUI of InstallAnywhere installers. Run this test just like any other JUnit.

1. Import Eclipse Project
-------------------------

When importing this project into your Eclipse workspace, please make sure you keep the current in
its current location (as opposed to copying it to the workspace's directory.)
Otherwise, the test case will not be able to find the installer generated in the step below.

2. Build BasicProject.iap_xml
-----------------------------

This is the InstallAnywhere project used to create the installer used by the JUnit test. 
Please build this project as a pure Java installer before running the JUnit test.

3. Run com.installanywhere.ia.automation.gui.samples.basic.BasicProjectAutomation
-----------------------------------------------------------------------------

This JUnit extends GUIAutomationFixture and leverages its protected methods to:
- Launch installers/uninstallers
- Wait for windows/panels
- Navigate panels/fields
- Type text in fields
- Assert that installers/uninstaller ran successfully (returning exit = 0)
- Assert that files have been installed/uninstalled succesfully.

Known Limitation
----------------

Currently, GUIAutomationFixture can only launch pure Java installers, which have some limitations,
such as the inability to create shortcuts and remove the uninstaller.jar from the install dir.

You should be able to extend GUIAutomationFixture to launch Windows native launchers by calling
Runtime.exec and passing the following argument to the installer: -gui_auto:12060
___________
package com.installanywhere.ia.automation.gui.samples.basic;

import java.awt.event.KeyEvent;
import java.io.File;
import java.io.IOException;

import com.zerog.ia.test.fixtures.GUIAutomationFixture;

public class BasicProjectAutomation extends GUIAutomationFixture {
    private static final String NEW_LOCATION = "newlocation";

    public void testInstallAndUninstall() throws IOException {
        String productName = "BasicProject";
        String buildOutput = "_Build_Output/Web_Installers/InstData/Java";
        File installer = new File("./" + productName + buildOutput, "install.jar");
        File installDir = new File("D:/test",NEW_LOCATION);

        setWaitTimeout(30 * SECONDS);

        runInstall(productName, installer);
        assertInstallerHasCompletedSuccessfully();
        assertTrue("Install dir should have been created.", installDir.exists());
        assertTrue("file-to-install.txt", new File(installDir, "file-to-install.txt").exists());

        runUninstall(installDir, productName);
        assertUninstallerHasCompletedSuccessfully();
        assertTrue("Install dir should be empty with the exception of the uninstall dir.",
                installDir.list().length == 1);
        assertFalse("file-to-install.txt should have been removed", new File(installDir,
                "file-to-install.txt").exists());
    }

    private void runInstall(String productName, File installer) {
        try {
            launchInstaller(installer.getPath());
            
            // Waits for unlicensed version alert.            
            // Remove this if the project was built with a licensed version of InstallAnywhere.
            waitForWindow("Alert");
            pressEnter();
            
            waitForWindow(productName);
            waitForPanel("Introduction");
            pressEnter();
    
            waitForPanel("Choose Install Folder");
            pressTab();
            for (int i = 0; i < productName.length(); i++) {
                pressKey(KeyEvent.VK_BACK_SPACE);
            }
            type(NEW_LOCATION);
            pressTab(); pressTab(); pressTab(); pressTab(); pressTab();
            pressEnter();
    
            waitForPanel("Pre-Installation Summary");
            pressEnter();
    
            waitForPanel("Installing " + productName);
    
            waitForPanel("Install Complete");
            pressEnter();
    
            waitForInstallerToComplete();
        } finally {
            printJavaOutput("Installer");
        }
    }
    
    private void runUninstall(File installDir, String productName) {
        try {
            launchUninstaller(installDir, productName);
            waitForWindow("Uninstall " + productName);

            waitForPanel("Uninstall " + productName);
            pressEnter();

            waitForPanel("Uninstall Options");
            pressEnter();

            waitForPanel("Uninstall Complete");
            pressEnter();

            waitForInstallerToComplete();
        } finally {
            printJavaOutput("Uninstaller");
        }
    }
}
